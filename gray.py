
import os 
import matplotlib.pyplot as plt 
from PIL import Image
import math
from skimage import io

from scipy.spatial import distance
from imgDist import ImgDist
from functions import *
from functions_calcs import *
import cv2 
import matplotlib.image as mpimg


#vetor que guarda a distância da comparação com as n imagens
vector_dist = [];

path = 'Vistex/';
pathHistograma='plots/'
#path="grupo1/"
files = os.listdir(path);
n = len(files);


file = open('Resultado_Gray.html', 'w')
counter = 0
images_normalized = []

isGray = True;

for i in range(n):
  image = cv2.imread(path + files[i])
  image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY);  
  
  #image_histogram = matrixImagem(image, isGray);
  image_histogram = cv2.calcHist([image], [0], None, [256], [0, 256])
  image_normalized = normaliza(image_histogram, image, isGray);
  inputImageName = files[i];
  plotGraphic(image_histogram, inputImageName)
  counter += 1;
 
  images_normalized.append(image_normalized)
  
  print("comparando com outras imagens " + str(round((counter/(n)),2)* 100) + "%");    

total_certas=0
length_images =len(images_normalized) 
for i in range(length_images):
  print("calculando... " + str(round((i/(length_images)),2)* 100) + "%"); 
  vector_dist = []
  for j in range(length_images):      
    dist = calculaDistancia(images_normalized[i], images_normalized[j]);  
    if(dist != 0):
      imgDist = ImgDist(dist, j)  
      vector_dist.append(imgDist);
  

  vect = []  
  vect = sorted(vector_dist, key=lambda imgDist: imgDist.dist)
  
  inputImageName = files[i];
  
  escreveArquivo("<h1> IMAGENS PARECIDAS COM "+ inputImageName +"</h1>", file)

  escreveArquivo("<h3> Imagem original: <img src="+ path + inputImageName + "> "+inputImageName+
  " Histograma <img src="+ pathHistograma + inputImageName + "> </h3> <br>", file)    
  
  groupImageReference = inputImageName.split("_",1)
  ocorrencias = 0
  #gerando arquivo com 3 imagens parecidas
  for k in range(3):         
    nomeArquivo = files[vect[k].index];
    grupo = nomeArquivo.split('_',1)
   
    if(groupImageReference[0] == grupo[0]):
      ocorrencias+= 1;
      
    
    escreveArquivo("<img src="+ path + nomeArquivo + " >"+nomeArquivo+"  Histograma <img src="+ pathHistograma + nomeArquivo + "> ", file)
    

  if(ocorrencias == 0):
    escreveArquivo("<h5 style='color:red;'> Classificação: 0 '%'de acerto com o grupo </h5>", file)
  else:
      if(ocorrencias == 3):
        total_certas +=1
        escreveArquivo("<h5 style='color:green;'> Classificação 100 '%'de acerto com o grupo </h5>", file)
      else:
        if(ocorrencias == 2):
          total_certas +=1

        escreveArquivo("<h5 style='color:pink;'> Classificação "+ str(round( (100/3)*ocorrencias,2)) +" '%' de acerto com o grupo </h5>", file)
      
print("arquivo html gerado")
print("total certas ", total_certas)

escreveArquivo("<h1 style='color:green;'> TOTAL CERTAS: "+  str(round((total_certas/n)*100,2))+ " '%'</h1>", file);

file.close()
  

#print(my_image_dist)

