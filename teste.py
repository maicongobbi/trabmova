
import os 
import matplotlib.pyplot as plt 
from PIL import Image
import math
from skimage import io

from scipy.spatial import distance
from imgDist import ImgDist
from functions import *
from functions_calcs import *
import cv2 
import matplotlib.image as mpimg

def plot(image):
  
  plt.imshow(image, cmap=plt.get_cmap('gray'), vmin=0, vmax=1)
  #image = io.imread(path, as_gray=isGray);  
  plt.show()

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])

path = 'Vistex/';
#path = 'grupo1/';
files = os.listdir(path);
path = path + files[10];

#################################################

#image = cv2.imread(path)
img = mpimg.imread(path)     

img = cv2.imread(path)
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #converte P&B
#Função calcHist para calcular o hisograma da imagem
h = cv2.calcHist([img], [0], None, [256], [0, 256])

plt.figure(figsize=(2, 1))
plt.title("Histograma Gray")
plt.xlabel("Intensidade")
plt.ylabel("Qtde de Pixels")
plt.plot(h)
plt.xlim([0, 256])
plt.savefig('plots/plot.png')
