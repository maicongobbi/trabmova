import numpy as np
import cv2
from functions_calcs import *
import matplotlib.pyplot as plt 


def criarArray():  
  return np.zeros((256), dtype=np.uint8); 

def matrixImagem(imgCalc, isGRay):
  matrix_gray = []    

  if(isGRay == True):    
    matrix_gray = imgCalc[:,:]            
  else:      
    matrix_red = imgCalc[:,:,0];
    matrix_green = imgCalc[:,:,1];
    matrix_blue = imgCalc[:,:,2];      

  histogram = criarArray();
  
  if(isGRay == True):
    w, h = imgCalc.shape;      
  else:
    w, h, _ = imgCalc.shape;      
  
  for i in range(w):
    for j in range(h):    

      if(isGRay == True):
        calcHistogram(histogram, int(matrix_gray[i, j]))                
      else:        
        calcHistogram(histogram, int(matrix_blue[i, j]))     
        calcHistogram(histogram, int(matrix_green[i, j]));
        calcHistogram(histogram, int(matrix_red[i, j]));
  
  #print(histogram); 
  return histogram;


  def matrixImagemGray(imgCalc):
    matrix_gray = imgCalc[:,:];    

    for i in range(w):
      for j in range(h):    
        calcHistogram(histogram, int(matrix_gray[i, j]))
    
  return histogram;

def escreveArquivo(mensagem, file):
  file.write(mensagem);


def plotGraphic(histogram,inputPlotName):
  plt.figure(figsize=(2, 1))
  plt.title("Histograma Gray")
  plt.xlabel("Intensidade")
  plt.ylabel("Qtde de Pixels")
  plt.plot(histogram)
  plt.xlim([0, 256])
  plt.savefig('plots/'+inputPlotName,)
  plt.close()
