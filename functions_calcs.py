from skimage import io
import os 
from functions import *

#calcula histograma da imagem, em cada posição encontrada, acrescenta-se 1 se encontrar
def calcHistogram(histogram, position):  
  
  histogram[position] += 1;
  return;

#divide cada posição do histograma pela qtd de pixels da imagem
def normaliza(histogram, img, isGray):  
  if(isGray == True):
    w, h = img.shape;      
  else:
    w, h, _ = img.shape;      
  qtdPixel = w*h;
  floats = []
  
  for i in range(len(histogram)):    
    calc = float(histogram[i])/qtdPixel;   
    #print(calc)  
    floats.append(float(calc))   
  
  return floats;      

#calcula a distância entre o vetor de comparação e os n vetores gerados, vetores já normalizados
def calculaDistancia(my_image_normalize, histogram):
  result = 0;
 # print(histogram)
  for i in range(len(histogram)):    
    dist = 0;  
    dist = my_image_normalize[i] - histogram[i];
    #dist = dist **2    
    if(dist < 0):
      dist = dist * -1;    
    result += dist; 
  
  return result; 
