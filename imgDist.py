class ImgDist:
  def __init__ (self, dist, index):
    self.dist = dist
    self.index = index;

    def setDist(self, d):
      self.dist = d;
    
    def setIndex(self, index):
      self.index = index;
    
    def getDist(self):
      return self.dist;
    
    def getIndex(self):
      return self.index;